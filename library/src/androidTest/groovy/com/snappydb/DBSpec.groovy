package com.snappydb

import android.content.Context
import com.andrewreitz.spock.android.WithContext
import com.snappydb.internal.DBImpl
import com.snappydb.model.SimpleObject
import spock.lang.Specification

/**
 * Copyright 2015 Ivan Usalko (Usalko.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class DBSpec extends Specification {

    @WithContext def Context context

    def setup() {
        DefaultMapper defaultMapper = new DefaultMapper()
        DBImpl.registerAdapter(defaultMapper, defaultMapper)
    }

    def "Simple run"() {
        DB db = DBFactory.open(context)
        db.register(SimpleObject.class)
        SimpleObject so = new SimpleObject()
        so.key = "A"
        so.value = "B"
        db.persist(so)
        expected:
            db.get("A") != null

        SimpleObject soPersisted = db.find("A")
        expected:
            soPersisted != null && "B".equals(soPersisted.value)

        SimpleObject soDel = new SimpleObject()
        soDel.key = "A"
        db.remove(soDel)
        when:
            db.get("A")

        then:
            StorageException ex = thrown()
            ex.message = "Can't find"

        expected:
            db.find("A") == null

    }
}

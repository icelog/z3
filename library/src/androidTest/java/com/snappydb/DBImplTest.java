package com.snappydb;

import android.test.suitebuilder.annotation.SmallTest;

import com.snappydb.model.NotKeyingObject;
import com.snappydb.model.NotKeyingObject2;
import com.snappydb.model.NotRegisteredObject;
import com.snappydb.model.NotRegisteredObject2;
import com.snappydb.model.QualifiedObject;
import com.snappydb.model.SimpleObject;
import com.snappydb.model.SimpleObject2;

import org.junit.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//import org.junit.Rule;
//import org.junit.rules.ExpectedException;

/**
 * Copyright 2015 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class DBImplTest extends Z3TestCase {

    //@Rule
    //public ExpectedException thrown = ExpectedException.none();

    @SmallTest
    public void testSimple() throws Throwable {
        DB db = DBFactory.open(getContext());
        db.register(SimpleObject.class);
        SimpleObject so = new SimpleObject();
        so.setKey0("A0");
        so.setValue("B");
        db.persist(so);
        Assert.assertNotNull(db.get(SimpleObject.class.getName() + ":A0::"));

        SimpleObject soPersisted = db.find(SimpleObject.class, "A0");
        Assert.assertNotNull(soPersisted);
        Assert.assertEquals("B", soPersisted.getValue());

        SimpleObject soDel = new SimpleObject();
        soDel.setKey0("A0");
        db.remove(soDel);
        try {
            Assert.assertNull(db.get(SimpleObject.class.getName() + ":A0::"));
        } catch (StorageException e) {
        }

        Assert.assertNull(db.find(SimpleObject.class, "A0"));
    }

    @SmallTest
    public void testSingleton() throws Throwable {
        DB db = DBFactory.open(getContext());
        db.register(NotKeyingObject.class);
        NotKeyingObject so = new NotKeyingObject();
        so.setValue("B");
        db.persist(so);
        Assert.assertNotNull(db.get(NotKeyingObject.class.getName()));

        NotKeyingObject soPersisted = db.find(NotKeyingObject.class);
        Assert.assertNotNull(soPersisted);
        Assert.assertEquals("B", soPersisted.getValue());

        NotKeyingObject soDel = new NotKeyingObject();
        db.remove(soDel);
        try {
            Assert.assertNull(db.get(NotKeyingObject.class.getName()));
        } catch (StorageException e) {
        }

        Assert.assertNull(db.find(NotKeyingObject.class));
    }

    @SmallTest
    public void testContains() throws Throwable {
        DB db = DBFactory.open(getContext());
        db.register(NotKeyingObject.class);
        NotKeyingObject o = new NotKeyingObject();
        o.setValue("B");

        Assert.assertFalse(db.contains(o));
        db.persist(o);
        Assert.assertTrue(db.contains(o));

        db.remove(o);
        Assert.assertFalse(db.contains(o));
    }

    @SmallTest
    public void testGetObject() throws Throwable {
        DB db = DBFactory.open(getContext());
        db.register(QualifiedObject.class);

        //Clear previous
        db.remove(QualifiedObject.DEFAULT);

        Assert.assertEquals(QualifiedObject.DEFAULT, db.getObject(QualifiedObject.DEFAULT));

        QualifiedObject o = new QualifiedObject();
        o.setValue("A");
        db.persist(o);
        Assert.assertEquals(o, db.getObject(QualifiedObject.DEFAULT));

        db.remove(o);
        Assert.assertEquals(QualifiedObject.DEFAULT, db.getObject(QualifiedObject.DEFAULT));
    }

    @SmallTest
    public void testFindAll() throws Throwable {
        DB db = DBFactory.open(getContext());
        removeAll(db, SimpleObject.class);

        db.register(SimpleObject.class);
        SimpleObject so = new SimpleObject();
        so.setKey0("A0");
        so.setValue("B");
        db.persist(so);

        List<SimpleObject> col0 = new ArrayList<>();
        for (SimpleObject o : db.findAll(SimpleObject.class, "A0")) {
            col0.add(o);
        }
        Assert.assertFalse(col0.isEmpty());
        Assert.assertEquals("B", col0.get(0).getValue());

        SimpleObject so2 = new SimpleObject();
        so2.setKey1("A1");
        so2.setValue("C");
        db.persist(so2);

        List<SimpleObject> col1 = new ArrayList<>();
        for (SimpleObject o : db.findAll(SimpleObject.class)) {
            col1.add(o);
        }
        Assert.assertTrue(col1.size() == 2);
        System.out.println(Arrays.toString(col1.toArray()));
    }

    @SmallTest
    public void testNotRegistered() throws Throwable {
        DB db = DBFactory.open(getContext());
        NotRegisteredObject uo = new NotRegisteredObject();
        uo.setKey0("A0");
        uo.setValue("B");
        db.persist(uo);
        Assert.assertNotNull(db.get(NotRegisteredObject.class.getName() + ":A0::"));

        NotRegisteredObject uoPersisted = db.find(NotRegisteredObject.class, "A0");
        Assert.assertNotNull(uoPersisted);
        Assert.assertEquals("B", uoPersisted.getValue());

        NotRegisteredObject uoRemoved = new NotRegisteredObject();
        uoRemoved.setKey0("A0");
        db.remove(uoRemoved);
        try {
            Assert.assertNull(db.get(NotRegisteredObject.class.getName() + ":A0::"));
        } catch (StorageException e) {
        }

        Assert.assertNull(db.find(NotRegisteredObject.class, "A0"));
    }

    @SmallTest
    public void testRegisterIsIdempotent1() throws Throwable {
        DB db = DBFactory.open(getContext());
        db.register(SimpleObject2.class);
        db.register(SimpleObject2.class);
        db.persist(new SimpleObject2());
    }

    @SmallTest
    public void testRegisterIsIdempotent2() throws Throwable {
        DB db = DBFactory.open(getContext());
        db.persist(new NotKeyingObject2());
        db.persist(new NotKeyingObject2());
        db.register(NotKeyingObject2.class);
    }

    @SmallTest
    public void testGetNotRegisteredObject() throws Throwable {
        DB db = DBFactory.open(getContext());
        removeAll(db, NotRegisteredObject2.class);
        //TODO Expected WorlflowException
        //Assert.assertNull(db.getObject(new NotRegisteredObject2()));
    }

    private void removeAll(DB db, Class<?> cl) throws StorageException {
        String[] keys = db.findKeys(cl.getName());
        if (keys == null) {
            return;
        }
        for (String key : keys) {
            db.del(key);
        }
    }
}
package com.snappydb.sample.tests.api.helper;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.icelog.z3.i4a.ArrayAndAnyObjectDeserializer;
import org.icelog.z3.i4a.ArrayAndAnyObjectSerializer;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collection;

/**
 * Copyright 2015 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class DefaultMapper implements ArrayAndAnyObjectDeserializer, ArrayAndAnyObjectSerializer {

    private final ObjectMapper om;

    public DefaultMapper(ObjectMapper om) {
        this.om = om;
    }

    @Override
    public <T extends Serializable> T getSerializable(byte[] data, Class<T> cl) throws Throwable {
        return om.readValue(data, cl);
    }

    @Override
    public <T> T getAnyObject(byte[] data, Class<T> cl) throws Throwable {
        return om.readValue(data, cl);
    }

    @Override
    public <C extends Collection, T> C getCollection(byte[] data, Class<C> collectionClass, Class<T> cl) throws Throwable {
        return om.readValue(data, om.getTypeFactory().constructCollectionType(collectionClass, cl));
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Serializable> T[] getArray(byte[] data, T[] array) throws Throwable {
        return (T[]) om.readValue(data, array.getClass());
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T[] getObjectArray(byte[] data, T[] array) throws Throwable {
        return (T[]) om.readValue(data, array.getClass());
    }

    @Override
    public void putObjectToStream(OutputStream stream, Object value) throws Throwable {
        om.writeValue(stream, value);
    }

    @Override
    public void putSerializableArrayToStream(ByteArrayOutputStream stream, Serializable[] value) throws Throwable {
        om.writeValue(stream, value);
    }

    @Override
    public void putAnyObjectArrayToStream(ByteArrayOutputStream stream, Object[] value) throws Throwable {
        om.writeValue(stream, value);
    }
}

package com.snappydb;

import android.test.AndroidTestCase;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.snappydb.internal.DBImpl;
import com.snappydb.sample.tests.api.helper.Book;
import com.snappydb.sample.tests.api.helper.DefaultMapper;

import org.icelog.clog.Clog;
import org.icelog.z3.i4a.ArrayAndAnyObjectDeserializer;
import org.icelog.z3.i4a.ArrayAndAnyObjectSerializer;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collection;

/**
 * Copyright 2015 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class Z3TestCase extends AndroidTestCase {

    private static final String TAG = Z3TestCase.class.getSimpleName();
    private static boolean inited;

    {
        inited = init();
    }

    private synchronized boolean init() {
        if (inited) {
            return true;
        }
        try {
            ObjectMapper om = new ObjectMapper();
            //Thread.currentThread().getContextClassLoader().loadClass(DefaultMapper.class.getName());
            DefaultMapper defaultMapper = new DefaultMapper(om);
            DBImpl.registerAdapter(defaultMapper, defaultMapper);
            return true;
        } catch (Throwable th) {
            Clog.e(TAG, th);
        }
        return false;
    }

}

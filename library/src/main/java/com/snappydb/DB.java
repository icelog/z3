/*
 * Copyright (C) 2013 Nabil HACHICHA.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.snappydb;

import org.icelog.z3.Storage;

import java.io.Serializable;
import java.util.Collection;

public interface DB extends Storage {
	//***********************
	//*      DB MANAGEMENT
	//***********************

    void close ()  throws StorageException;

	void destroy ()  throws StorageException;

    boolean isOpen ()  throws StorageException;

	//***********************
	//*      CREATE
	//***********************
	void put (String key, byte[] data) throws StorageException;

	void put (String key, String value) throws StorageException;
	
	void put (String key, Serializable value) throws StorageException;
	
	void put (String key, Serializable [] value) throws StorageException;

    void put (String key, Object object) throws StorageException;

    void put (String key, Object [] object) throws StorageException;
	
	void putInt (String key, int val) throws StorageException;
	
	void putShort (String key, short val) throws StorageException;
	
	void putBoolean (String key, boolean val) throws StorageException;
	
	void putDouble (String key, double val) throws StorageException;
	
	void putFloat (String key, float val) throws StorageException;
		
	void putLong (String key, long val) throws StorageException;

	//***********************
	//*      DELETE 
	//***********************
	void del(String key)  throws StorageException;

	//***********************
	//*      RETRIEVE 
	//***********************	
	String get(String key)  throws StorageException;
	
	byte[] getBytes(String key)  throws StorageException;
	
	<T extends Serializable> T get(String key, Class<T> className) throws StorageException;

	<T> T getObject(String key, Class<T> className) throws StorageException;

	<C extends Collection, T> C getCollection(String key, Class<C> collectionClass, Class<T> className) throws StorageException;

	<T extends Serializable> T[] getArray (String key, Class<T> className) throws StorageException;

    <T> T[] getObjectArray (String key, Class<T> className) throws StorageException;
	
	short getShort(String key)  throws StorageException;
	
	int getInt(String key)  throws StorageException;
	
	boolean getBoolean(String key)  throws StorageException;
	
	double getDouble(String key)  throws StorageException;
	
	long getLong(String key)  throws StorageException;
	
	float getFloat(String key)  throws StorageException;

	//****************************
	//*      KEYS OPERATIONS 
	//****************************	
	boolean exists (String key) throws StorageException;

    String[] findKeys(String prefix) throws StorageException;
    String[] findKeys(String prefix, int offset) throws StorageException;
    String[] findKeys(String prefix, int offset, int limit) throws StorageException;

    int countKeys(String prefix) throws StorageException;

    String[] findKeysBetween(String startPrefix, String endPrefix) throws StorageException;
    String[] findKeysBetween(String startPrefix, String endPrefix, int offset) throws StorageException;
    String[] findKeysBetween(String startPrefix, String endPrefix, int offset, int limit) throws StorageException;

    int countKeysBetween(String startPrefix, String endPrefix) throws StorageException;

    //***********************
    //*      ITERATORS
    //***********************
    KeyIterator allKeysIterator() throws StorageException;
    KeyIterator allKeysReverseIterator() throws StorageException;

    KeyIterator findKeysIterator(String prefix) throws StorageException;
    KeyIterator findKeysReverseIterator(String prefix) throws StorageException;

    KeyIterator findKeysBetweenIterator(String startPrefix, String endPrefix) throws StorageException;
    KeyIterator findKeysBetweenReverseIterator(String startPrefix, String endPrefix) throws StorageException;

}


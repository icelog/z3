/*
 * Copyright (C) 2013 Nabil HACHICHA.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.snappydb.internal;

import android.text.TextUtils;

import com.snappydb.AdapterException;
import com.snappydb.DB;
import com.snappydb.KeyIterator;
import com.snappydb.StorageException;
import com.snappydb.WorkflowException;

import org.icelog.z3.i4a.ArrayAndAnyObjectDeserializer;
import org.icelog.z3.i4a.ArrayAndAnyObjectSerializer;
import org.icelog.z3.Key;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

public class DBImpl implements DB {
    private static final String LIB_NAME = "z3-native";
    private static final int LIMIT_MAX = Integer.MAX_VALUE - 8;

    private String dbPath;

    private ArrayAndAnyObjectDeserializer deserializer;
    private ArrayAndAnyObjectSerializer serializer;

    private static ArrayAndAnyObjectDeserializer defaultDeserializer;
    private static ArrayAndAnyObjectSerializer defaultSerializer;

    private class AutoKeyingMap {
        private final Map<Class<?>, List<Field>> map;
        private final ReentrantLock lock;

        public AutoKeyingMap() {
            this.map = new HashMap<>();
            this.lock = new ReentrantLock();
        }

        public void addField(Class<?> cl, Field f) {
            if (f == null || cl == null) {
                throw new IllegalArgumentException("Field and class must be not null!");
            }
            lock.lock();
            try {
                List<Field> fields = map.get(cl);
                if (fields == null) {
                    fields = new ArrayList<>();
                    map.put(cl, fields);
                }
                fields.add(f);
            } finally {
                lock.unlock();
            }
        }

        public void addSingleton(Class<?> cl) {
            if (cl == null) {
                throw new IllegalArgumentException("Class must be not null!");
            }
            lock.lock();
            try {
                List<Field> fields = map.get(cl);
                if (fields != null && fields.size() > 1) {
                    throw new WorkflowException("Field set not empty");
                }
                map.put(cl, Collections.<Field>emptyList());
            } finally {
                lock.unlock();
            }
        }

        public String getKey(Object object) throws IllegalAccessException {
            if (object == null) {
                return "";
            }
            lock.lock();
            try {
                Class<?> aClass = object.getClass();
                List<Field> fields = map.get(aClass);
                if (fields == null) {
                    throw new WorkflowException(getNotRegisteredMessage(aClass));
                }
                StringBuilder text = new StringBuilder(aClass.getName());
                for (Field f : fields) {
                    f.setAccessible(true);
                    Object value = f.get(object);
                    String key = value != null ? String.valueOf(value) : "";
                    text.append(':').append(key);
                }
                return text.toString();
            } finally {
                lock.unlock();
            }
        }

        public String getKey(Class<?> objectClass, Object... keys) {
            if (objectClass == null) {
                return "";
            }
            lock.lock();
            try {
                List<Field> fields = map.get(objectClass);
                if (fields == null) {
                    throw new WorkflowException(getNotRegisteredMessage(objectClass));
                }
                StringBuilder text = new StringBuilder(objectClass.getName());
                int len = fields.size();
                for (int i = 0; i < len; i++) {
                    final String key;
                    if (i < keys.length) {
                        key = keys[i] != null ? String.valueOf(keys[i]) : "";
                    } else {
                        key = "";
                    }
                    text.append(':').append(key);
                }
                return text.toString();
            } finally {
                lock.unlock();
            }
        }

        public String getPrefix(Class<?> objectClass, Object... keys) {
            if (objectClass == null) {
                return "";
            }
            lock.lock();
            try {
                List<Field> fields = map.get(objectClass);
                if (fields == null) {
                    throw new WorkflowException(getNotRegisteredMessage(objectClass));
                }
                StringBuilder text = new StringBuilder(objectClass.getName());
                int len = fields.size();
                for (int i = 0; i < len; i++) {
                    final String key;
                    if (i < keys.length) {
                        key = keys[i] != null ? String.valueOf(keys[i]) : "";
                    } else {
                        key = "";
                    }
                    if (key.isEmpty()) {
                        break;
                    }
                    text.append(':').append(key);
                }
                return text.toString();
            } finally {
                lock.unlock();
            }
        }

        public boolean isRegistered(Class<?> objectClass) {
            if (objectClass == null) {
                return false;
            }
            lock.lock();
            try {
                return map.containsKey(objectClass);
            } finally {
                lock.unlock();
            }
        }

    }

    private String getNotRegisteredMessage(Class<?> objectClass) {
        return "Class [" + objectClass + "] not registered, try to register or persist before";
    }

    private AutoKeyingMap autoKeyingMap;

    static {
        System.loadLibrary(LIB_NAME);
    }

    public static void registerAdapter(ArrayAndAnyObjectDeserializer deserializer, ArrayAndAnyObjectSerializer serializer) {
        defaultDeserializer = deserializer;
        defaultSerializer = serializer;
    }

    public DBImpl(String path) throws StorageException {
        this(path, defaultDeserializer, defaultSerializer);
    }

    public DBImpl(String path, ArrayAndAnyObjectDeserializer deserializer, ArrayAndAnyObjectSerializer serializer) throws StorageException {
        this.dbPath = path;
        this.deserializer = deserializer;
        this.serializer = serializer;

        __open(dbPath);
    }

    // ***********************
    // *     DB MANAGEMENT
    // ***********************

    @Override
    public void close() {
        __close();
    }

    @Override
    public void destroy() throws StorageException {
        __destroy(dbPath);
    }

    @Override
    public boolean isOpen() throws StorageException {
        return __isOpen();
    }


    // ***********************
    // *       CREATE
    // ***********************
    @Override
    public void put(String key, String value) throws StorageException {
        checkArgs(key, value);

        __put(key, value);
    }

    @Override
    public void put(String key, Serializable value) throws StorageException {
        checkArgs(key, value);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            serializer.putObjectToStream(stream, value);
            __put(key, stream.toByteArray());

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Can't put value to storage ", th);
        }
    }

    @Override
    public void put(String key, Object value) throws StorageException {
        checkArgs(key, value);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            serializer.putObjectToStream(stream, value);
            __put(key, stream.toByteArray());

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Can't put to a storage", th);
        }
    }

    @Override
    public void put(String key, byte[] value) throws StorageException {
        checkArgs(key, value);

        __put(key, value);
    }


    @Override
    public void put(String key, Serializable[] value) throws StorageException {
        checkArgs(key, value);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            serializer.putSerializableArrayToStream(stream, value);
            __put(key, stream.toByteArray());

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Can't put serializable array to storage ", th);
        }
    }

    @Override
    public void put(String key, Object[] value) throws StorageException {
        checkArgs(key, value);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            serializer.putAnyObjectArrayToStream(stream, value);
            __put(key, stream.toByteArray());

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Can't put any object array to stream ", th);
        }
    }

    @Override
    public void putShort(String key, short val) throws StorageException {
        checkKey(key);

        __putShort(key, val);
    }


    @Override
    public void putInt(String key, int val) throws StorageException {
        checkKey(key);

        __putInt(key, val);
    }


    @Override
    public void putBoolean(String key, boolean val) throws StorageException {
        checkKey(key);

        __putBoolean(key, val);
    }

    @Override
    public void putDouble(String key, double val) throws StorageException {
        checkKey(key);

        __putDouble(key, val);
    }

    @Override
    public void putFloat(String key, float val) throws StorageException {
        checkKey(key);

        __putFloat(key, val);
    }

    @Override
    public void putLong(String key, long val) throws StorageException {
        checkKey(key);

        __putLong(key, val);
    }

    @Override
    public void persist(Object object) {
        AutoKeyingMap akMap = getAutoKeyingMap();
        if (akMap == null) {
            throw new WorkflowException("Not initialized, please register class before usage this method");
        }
        try {
            Class<?> aClass = object.getClass();
            if (!akMap.isRegistered(aClass)) {//Auto registering feature
                register(aClass);
            }
            String key = akMap.getKey(object);
            put(key, object);
        } catch (IllegalAccessException | StorageException e) {
            throw new WorkflowException("Can't persist object [" + object + "]", e);
        }
    }

    // ***********************
    // *      DELETE
    // ***********************

    @Override
    public void del(String key) throws StorageException {
        checkKey(key);

        __del(key);
    }

    @Override
    public void remove(Object object) {
        if (autoKeyingMap == null) {
            throw new WorkflowException("Not initialized, please register class before usage this method");
        }
        try {
            String key = autoKeyingMap.getKey(object);
            del(key);
        } catch (StorageException | IllegalAccessException e) {
            throw new WorkflowException("Can't remove object [" + object + "]", e);
        }
    }

    // ***********************
    // *       RETRIEVE
    // ***********************

    @Override
    public <T extends Serializable> T get(String key, Class<T> className)
            throws StorageException {
        checkArgs(key, className);

        if (className.isArray()) {
            throw new StorageException(
                    "You should call getArray instead");
        }

        byte[] data = getBytes(key);

        try {
            return deserializer.getSerializable(data, className);

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Maybe you tried to retrieve an array using this method ? " +
                    "please use getArray instead ", th);
        } finally {
        }
    }

    @Override
    public <T> T getObject (String key, Class<T> cl)
            throws StorageException {
        checkArgs(key, cl);

        if (cl.isArray()) {
            throw new StorageException(
                    "You should call getObjectArray instead");
        }

        byte[] data = getBytes(key);

        try {
            return deserializer.getAnyObject(data, cl);

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Maybe you tried to retrieve an array using this method ? " +
                    "please use getObjectArray instead ", th);
        } finally {
        }
    }

    @Override
    public <C extends Collection, T> C getCollection (String key, Class<C> collectionClass, Class<T> cl)
            throws StorageException {
        checkArgs(key, cl);

        if (cl.isArray()) {
            throw new StorageException(
                    "You should call getObjectArray instead");
        }

        byte[] data = getBytes(key);

        try {
            return deserializer.getCollection(data, collectionClass, cl);

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Maybe you tried to retrieve an array using this method ? " +
                    "please use getObjectArray instead ", th);
        } finally {
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Serializable> T[] getArray(String key, Class<T> cl)
            throws StorageException {
        checkArgs(key, cl);

        byte[] data = __getBytes(key);

        T[] array = (T[]) Array.newInstance(cl, 0);

        try {
            return deserializer.getArray(data, array);

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Maybe you tried to retrieve an array using this method " +
                    "? please use getArray instead ", th);
        } finally {
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T[] getObjectArray(String key, Class<T> className)
            throws StorageException {
        checkArgs(key, className);

        byte[] data = __getBytes(key);

        T[] array = (T[]) Array.newInstance(className, 0);

        try {
            return deserializer.getObjectArray(data, array);

        } catch (Throwable th) {
            th.printStackTrace();
            throw new AdapterException("Maybe you tried to retrieve an array using this method " +
                    "? please use getArray instead ", th);
        } finally {
        }
    }

    @Override
    public byte[] getBytes(String key) throws StorageException {
        checkKey(key);

        return __getBytes(key);
    }

    @Override
    public String get(String key) throws StorageException {
        checkKey(key);

        return __get(key);
    }

    @Override
    public short getShort(String key) throws StorageException {
        checkKey(key);

        return __getShort(key);
    }

    @Override
    public int getInt(String key) throws StorageException {
        checkKey(key);

        return __getInt(key);
    }

    @Override
    public boolean getBoolean(String key) throws StorageException {
        checkKey(key);

        return __getBoolean(key);
    }

    @Override
    public double getDouble(String key) throws StorageException {
        checkKey(key);

        return __getDouble(key);
    }

    @Override
    public long getLong(String key) throws StorageException {
        checkKey(key);

        return __getLong(key);
    }

    @Override
    public float getFloat(String key) throws StorageException {
        checkKey(key);

        return __getFloat(key);
    }

    //****************************
    //*      KEYS OPERATIONS
    //****************************
    @Override
    public boolean exists(String key) throws StorageException {
        checkKey(key);

        return __exists(key);
    }

    @Override
    public String[] findKeys(String prefix) throws StorageException {
        return findKeys(prefix, 0, LIMIT_MAX);
    }

    @Override
    public String[] findKeys(String prefix, int offset) throws StorageException {
        return findKeys(prefix, offset, LIMIT_MAX);
    }

    @Override
    public String[] findKeys(String prefix, int offset, int limit) throws StorageException {
        checkPrefix(prefix);
        checkOffsetLimit(offset, limit);

        return __findKeys(prefix, offset, limit);
    }

    @Override
    public int countKeys(String prefix) throws StorageException {
        checkPrefix(prefix);

        return __countKeys(prefix);
    }

    @Override
    public String[] findKeysBetween(String startPrefix, String endPrefix)
            throws StorageException {
        return findKeysBetween(startPrefix, endPrefix, 0, LIMIT_MAX);
    }

    @Override
    public String[] findKeysBetween(String startPrefix, String endPrefix, int offset)
            throws StorageException {
        return findKeysBetween(startPrefix, endPrefix, offset, LIMIT_MAX);
    }

    @Override
    public String[] findKeysBetween(String startPrefix, String endPrefix, int offset, int limit)
            throws StorageException {
        checkRange(startPrefix, endPrefix);
        checkOffsetLimit(offset, limit);

        return __findKeysBetween(startPrefix, endPrefix, offset, limit);
    }

    @Override
    public int countKeysBetween(String startPrefix, String endPrefix)
            throws StorageException {
        checkRange(startPrefix, endPrefix);

        return __countKeysBetween(startPrefix, endPrefix);
    }

    @Override
    public <T> T find(Class<T> objectClass, Object... keys) {
        if (autoKeyingMap == null) {
            throw new WorkflowException("Not initialized, please register class before usage this method");
        }
        try {
            String key = autoKeyingMap.getKey(objectClass, keys);
            return getObject(key, objectClass);
        } catch (AdapterException e) {
            throw new WorkflowException("Can't find object for class [" + objectClass + "] by keys [" + Arrays.toString(keys) + "]", e);
        } catch (StorageException e) {
            return null;
        }
    }

    @Override
    public boolean contains(Object object) {
        if (autoKeyingMap == null) {
            return false;
        }
        try {
            String key = autoKeyingMap.getKey(object);
            return get(key) != null;
        } catch (IllegalAccessException | StorageException e) {
            return false;
        }
    }

    @Override
    public <T> List<T> findAll(Class<T> objectClass, Object... keys) {
        if (autoKeyingMap == null) {
            return Collections.emptyList();
        }
        try {
            String prefix = autoKeyingMap.getPrefix(objectClass, keys);
            String[] existKeys = findKeys(prefix);
            if (existKeys == null || existKeys.length == 0) {
                return Collections.emptyList();
            }
            List<T> list = new ArrayList<>();
            for (String ek : existKeys) {
                list.add(getObject(ek, objectClass));
            }
            return list;
        } catch (AdapterException e) {
            throw new WorkflowException("Can't find all objects for class [" + objectClass + "] by keys [" + Arrays.toString(keys) + "]", e);
        } catch (StorageException e) {
            return Collections.emptyList();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getObject(T defaultValue, Object... keys) {
        if (defaultValue == null) {
            throw new IllegalStateException("Default value can't be null");
        }
        if (autoKeyingMap == null) {
            return defaultValue;
        }
        T t = (T) find(defaultValue.getClass(), keys);
        return t != null ? t : defaultValue;
    }

    //***********************
    //*      ITERATORS
    //***********************
    @Override
    public KeyIterator allKeysIterator()
            throws StorageException {
        return new KeyIteratorImpl(this, __findKeysIterator(null, false), null, false);
    }

    @Override
    public KeyIterator allKeysReverseIterator()
            throws StorageException {
        return new KeyIteratorImpl(this, __findKeysIterator(null, true),  null, true);
    }

    @Override
    public KeyIterator findKeysIterator(String prefix)
            throws StorageException {
        return new KeyIteratorImpl(this, __findKeysIterator(prefix, false), null, false);
    }

    @Override
    public KeyIterator findKeysReverseIterator(String prefix)
            throws StorageException {
        return new KeyIteratorImpl(this, __findKeysIterator(prefix, true), null, true);
    }

    @Override
    public KeyIterator findKeysBetweenIterator(String startPrefix, String endPrefix)
            throws StorageException {
        return new KeyIteratorImpl(this, __findKeysIterator(startPrefix, false), endPrefix, false);
    }

    @Override
    public KeyIterator findKeysBetweenReverseIterator(String startPrefix, String endPrefix)
            throws StorageException {
        return new KeyIteratorImpl(this, __findKeysIterator(startPrefix, true), endPrefix, true);
    }

    // ***********************
    // *      UTILS
    // ***********************

    private void checkArgs (String key, Object value) throws StorageException {
        checkArgNotEmpty (key, "Key must not be empty");

        if (null == value) {
            throw new StorageException("Value must not be empty");
        }
    }

    private void checkPrefix (String prefix) throws StorageException {
        checkArgNotEmpty (prefix, "Starting prefix must not be empty");
    }

    private void checkRange (String startPrefix, String endPrefix) throws StorageException {
        checkArgNotEmpty (startPrefix, "Starting prefix must not be empty");
        checkArgNotEmpty (startPrefix, "Ending prefix must not be empty");
    }

    private void checkKey (String key) throws StorageException {
        checkArgNotEmpty (key, "Key must not be empty");
    }

    private void checkArgNotEmpty (String arg, String errorMsg) throws StorageException {
        if (TextUtils.isEmpty(arg)) {
            throw new StorageException(errorMsg);
        }
    }

    private void checkOffsetLimit (int offset, int limit) throws StorageException {
        if (offset < 0) {
            throw new StorageException("Offset must not be negative");
        }
        if (limit <= 0) {
            throw new StorageException("Limit must not be 0 or negative");
        }
    }

    @Override
    public void register(Class<?> cl) {
        if (cl == null) {
            return;
        }
        AutoKeyingMap map = getAutoKeyingMap();
        if (map.isRegistered(cl)) {//Always be registered
            System.err.println("Class [" + cl.getName() + "] always be registered, nothing do");
            return;
        }
        Field[] fields = cl.getDeclaredFields();
        int keyFieldsCount = 0;
        for (Field f : fields) {
            Key key = f.getAnnotation(Key.class);
            if (key != null) {
                map.addField(cl, f);
                keyFieldsCount++;
            }
        }
        if (keyFieldsCount == 0) {//Singleton case
            map.addSingleton(cl);
        }
    }

    private AutoKeyingMap getAutoKeyingMap() {
        if (autoKeyingMap != null) {
            return autoKeyingMap;
        }
        synchronized (this) {
            if (autoKeyingMap == null) {
                autoKeyingMap = new AutoKeyingMap();
            }
            return autoKeyingMap;
        }
    }

    // native code
    private native void __close();

    private native void __open(String dbName) throws StorageException;

    private native void __destroy(String dbName) throws StorageException;

    private native boolean __isOpen() throws StorageException;

    private native void __put(String key, byte[] value) throws StorageException;

    private native void __put(String key, String value) throws StorageException;

    private native void __putShort(String key, short val) throws StorageException;

    private native void __putInt(String key, int val) throws StorageException;

    private native void __putBoolean(String key, boolean val) throws StorageException;

    private native void __putDouble(String key, double val) throws StorageException;

    private native void __putFloat(String key, float val) throws StorageException;

    private native void __putLong(String key, long val) throws StorageException;

    private native void __del(String key) throws StorageException;

    private native byte[] __getBytes(String key) throws StorageException;

    private native String __get(String key) throws StorageException;

    private native short __getShort(String key) throws StorageException;

    private native int __getInt(String key) throws StorageException;

    private native boolean __getBoolean(String key) throws StorageException;

    private native double __getDouble(String key) throws StorageException;

    private native long __getLong(String key) throws StorageException;

    private native float __getFloat(String key) throws StorageException;

    private native boolean __exists(String key) throws StorageException;

    private native String[] __findKeys (String prefix, int offset, int limit) throws StorageException;

    private native int __countKeys (String prefix) throws StorageException;

    private native String[] __findKeysBetween(String startPrefix, String endPrefix, int offset, int limit) throws StorageException;

    private native int __countKeysBetween(String startPrefix, String endPrefix) throws StorageException;

    native long __findKeysIterator(String prefix, boolean reverse) throws StorageException;

    native String[] __iteratorNextArray(long ptr, String endPrefix, boolean reverse, int max) throws StorageException;

    native boolean __iteratorIsValid(long ptr, String endPrefix, boolean reverse);

    native void __iteratorClose(long ptr);
}

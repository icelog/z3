package org.icelog.z3;

import java.util.List;

/**
 * Copyright 2015 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public interface Storage {

    /**
     * Put object, class of this object should be annotated by @Key.
     * That's fields values used as key.
     * If class not registered throw WorkflowException
     */
    void persist(Object object);

    /**
     * Delete object, class of this object should be annotated by @Key.
     * That's fields values used as key.
     * If class not registered throw WorkflowException
     */
    void remove(Object object);
    //<T> void delAll (Class<T> cl, Object... keys) throws StorageException;

    /**
     * Find object, class of this object should be annotated by @Key and registered sooner.
     * Keys can be null and contains values for annotated fields.
     * @return first found object
     */
    <T> T find(Class<T> objectClass, Object... keys);

    /**
     * Find all objects
     */
    <T> List<T> findAll (Class<T> objectClass, Object... keys);

    /**
     * If storage contains object, return true and false otherwise.
     */
    boolean contains(Object object);

    /**
     * Always return not null object, return persisted value or defaultValue if persisted value not found.
     * Throw IllegalArgumentException if defaultValue is null.
     */
    <T> T getObject(T defaultValue, Object... keys);

    //*********************************
    //*      AUTO-KEYS REGISTRATION
    //*********************************

    /**
     * Register class for usage auto-keying feature.
     * @param cl which have field should be annotated with @Key annotation
     */
    void register(Class<?> cl);

}

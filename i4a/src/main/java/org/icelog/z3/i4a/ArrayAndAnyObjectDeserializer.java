package org.icelog.z3.i4a;

import java.io.Serializable;
import java.util.Collection;

/**
 * Copyright 2015 Ivan Usalko (Usalko.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public interface ArrayAndAnyObjectDeserializer {

    <T extends Serializable> T getSerializable(byte[] data, Class<T> cl) throws Throwable;

    <T> T getAnyObject(byte[] data, Class<T> cl) throws Throwable;

    <C extends Collection, T> C getCollection(byte[] data, Class<C> collectionClass, Class<T> cl) throws Throwable;

    <T extends Serializable> T[] getArray(byte[] data, T[] array) throws Throwable;

    <T> T[] getObjectArray(byte[] data, T[] array) throws Throwable;
}

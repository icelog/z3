package org.icelog.z3.i4a;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.Serializable;

/**
 * Copyright 2015 Ivan Usalko (Usalko.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public interface ArrayAndAnyObjectSerializer {

    void putObjectToStream(OutputStream stream, Object value) throws Throwable;

    void putSerializableArrayToStream(ByteArrayOutputStream stream, Serializable[] value) throws Throwable;

    void putAnyObjectArrayToStream(ByteArrayOutputStream stream, Object[] value) throws Throwable;

}
